import json

class Controller :  

    def __init__(self, track) :
        self.lap = 0
        self.piece = 0
        self.position = 0
        self.velocity = 0
        self.turbo = False          # turbo available?
        self.clear4takeoff = False  # OK 2 use turbo?
        self.tick = 0
        self.end_turbo = 0
        self.max = 1.0
        self.min = 0.0
        self.reset_switch_to_lane()
        self.angle = 0.0
        self.track_piece = []
        self.throttle = 1.0
        self.ease_off = 0.5
        self.mash_down = 0.125

#        if track == 'keimola' :
#            self.max_velocity = 6.7 # keimola (min radius 100)
#        elif track == 'usa' :
#            self.max_velocity = 8.9775 # usa (min radius 200)
#        elif track == 'france' :
#            self.max_velocity = 4.6875 # france (min radius 50)
#        else :
#            self.max_velocity = 4.675 # germany (min radius 50)

    def max_velocity(self, radius) :
        if radius <= 50.0 :
            return 4.0
        if radius <= 100.0 :
            return 6.0
        if radius <= 200.0 :
            return 8.9
        return 20.0
        
    def throttle_value(self) :
        return self.throttle
        
    def use_turbo(self) :
        return self.turbo and self.clear4takeoff
        
    def reset_turbo(self) :
        self.turbo = False
        self.clear4takeoff = False
        
    def lane_switch(self) :
        return self.switch_to_lane
        
    def reset_switch_to_lane(self) :
        self.switch_to_lane = None
        
    def increase(self, amount) :   
        if (self.throttle < self.max) : 
            self.throttle += amount
            if (self.throttle >= self.max) :
                self.throttle = self.max
                #print('floored')
        
    def decrease(self, amount) :
        if self.throttle > self.min :
            self.throttle -= amount
            if self.throttle < self.min :
                self.throttle = self.min
                #print('coasting')
        
    def set_throttle(self, value) :
        if value >= self.min and value <= self.max :
            self.throttle = value
        
    # TO DO: use data
    def set_turbo(self, data) :
        self.turbo = True
        
    # encode the track as (type, length, max_velocity, switch)
    def load_track(self, data) :
        pieces = data['race']['track']['pieces']
        self.track_len = len(pieces)
        for piece in pieces :
            # print( piece )
            k, v = piece.keys(), piece.values()
            if k[0] == u'length' :
                self.track_piece.append(('S', v[0], self.max_velocity(1000), len(k) > 1))
            else :
                if len(k) > 2 :
                    switch = True
                    radius = v[2]
                else :
                    switch = False
                    radius = v[1]
                if v[0] > 0 :
                    self.track_piece.append(('R', v[0], self.max_velocity(radius), switch))
                else :
                    self.track_piece.append(('L', v[0], self.max_velocity(radius), switch))
        self.analyze_track()
                        
    # try to figure out lane switches to get the "inside" lane
    def analyze_track(self) :                        
        left_count = 0
        right_count = 0
        switches = []
        self.switches = []
        for piece in reversed(self.track_piece) :
            if (piece[0] == 'l') or (piece[0] == 'L') :
                left_count += 1
            elif (piece[0] == 'r') or (piece[0] == 'R') :
                right_count += 1
            if piece[3] :
                if right_count > left_count :
                    switches.append('Right')
                elif right_count == left_count :
                    switches.append(None)
                else :
                    switches.append('Left')
                left_count, right_count = 0, 0
            else :
                switches.append(None)
        # shift the switch commands ahead of the switch piece
        for i in reversed(range(self.track_len)) :
            self.switches.append(switches[(i-1)%self.track_len])
        #self.debug_track()

    # debug
    def debug_track(self) :
        i = 0
        for piece in self.track_piece :
            print(piece, self.switches[i])
            i += 1
                
    def set_switch(self) :
        self.switch_to_lane = self.switches[self.piece]
        
    def update_pos_vel(self) :
        # TO DO
        pass
            
    def react_to(self, data) :
        d0 = data[0]
        if d0['id']['name'] == 'Polycopter' :
            lap = d0['piecePosition']['lap']
            piece = d0['piecePosition']['pieceIndex']
            dist = d0['piecePosition']['inPieceDistance']
            # TO DO: fix 
            if (dist - self.position) > 0.0 :
                self.velocity = dist - self.position
            angle = d0['angle']
            slane = d0['piecePosition']['lane']['startLaneIndex']
            elane = d0['piecePosition']['lane']['endLaneIndex']
            print('cp: ', lap, piece, dist, angle, slane, elane, self.throttle, self.velocity)
            # TO DO: fix
            self.position = dist
            self.sldohff(lap, piece, angle)
        else :
            # TO DO: upd8 other car positions
            pass

    # smells like danger
    # ... or, heavily fried food
    def sldohff(self, lap, piece, angle) :
        # look 2 pieces ahead
        vel1 = self.track_piece[(piece+1) % self.track_len][2]
        vel2 = self.track_piece[(piece+2) % self.track_len][2]
        if self.velocity > vel1 or self.velocity > vel2 :
            # slow down
            self.decrease(self.ease_off)           
        else :
            # otherwise, speed up
            self.increase(self.mash_down)
        if piece > self.piece :
            # TO DO: self.position += 
            self.piece = piece
            self.set_switch()
            next = (piece+1) % self.track_len
            second = (piece+2) % self.track_len
            # only use turbo if at least 2 straight pieces ahead
            self.clear4takeoff = (self.track_piece[piece][0] == 'S') and (self.track_piece[next][0] == 'S') and (self.track_piece[second][0] == 'S')
        if lap > self.lap :
            self.lap = lap
            self.piece = 0
        # if angle increases...    
        #if angle > self.angle :
            # ...slow down
        #    self.decrease(self.ease_off)           
        #else :
            # ...otherwise, speed up
        #    self.increase(self.mash_down)
        # update angle    
        self.angle = angle
    

