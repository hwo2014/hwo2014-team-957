import json
import socket
import sys
import racecontrol

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        print(self.name)
        self.key = key
        #elf.track = "usa"
        #self.track = "germany"
        self.track = "keimola"
        #self.track = "france"
        self.race_control = racecontrol.Controller(self.track)

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
#        return self.msg("createRace", {"botId": {"name": self.name,
#                                                 "key": self.key},
#                                       "trackName": self.track,
#                                       "carCount": 1})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined: ", data)
        self.ping()

    def on_game_init(self, data):
        #print("track specified: ", data)
        # figure out lane switch & throttle strategy
        self.race_control.load_track(data)
        self.ping()

    def on_game_start(self, data):
        print("Race started: ", data)
        self.ping()

    def on_car_positions(self, data):
        self.race_control.react_to(data)
        new_lane = self.race_control.lane_switch()
        if self.race_control.use_turbo() :
            # don't use turbo when the throttle is too high
            if self.race_control.throttle_value() <= 0.5 :
                self.msg("turbo", "zoom")
                self.race_control.reset_turbo()      
            else :
                self.throttle(self.race_control.throttle_value())
        elif new_lane is not None :
            print('switching: ' + str(new_lane))
            self.msg("switchLane", new_lane)
            self.race_control.reset_switch_to_lane()
        else :
            self.throttle(self.race_control.throttle_value())

    def on_crash(self, data):
        print(self.name + " crashed")
        self.race_control.set_throttle(1.0)
        self.race_control.velocity = 0
        self.ping()

    def on_spawn(self, data):
        print(self.name + " spawned")
        self.race_control.set_throttle(1.0)
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()
        
    def on_turbo_available(self, data) :
        self.race_control.set_turbo(data)

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            #'yourCar': self.set_car
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'turboAvailable': self.on_turbo_available,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        
        debug_log = []
        
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            debug_log.append((msg_type, data))
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0} with {1}".format(msg_type, data))
                self.ping()
            line = socket_file.readline()
            
        #self.debug_race(debug_log)
            
    def debug_race(self, debug_log) :            
        # document the whole race    
        for message in debug_log :
        	if message[0] == u'carPositions' :
        	    d0 = message[1][0]
        	    lap = d0['piecePosition']['lap']
        	    piece = d0['piecePosition']['pieceIndex']
        	    dist = d0['piecePosition']['inPieceDistance']
        	    angle = d0['angle']
        	    slane = d0['piecePosition']['lane']['startLaneIndex']
        	    elane = d0['piecePosition']['lane']['endLaneIndex']
        	    print('cp: ', lap, piece, dist, angle, slane, elane)
        	else :
        	    print(message[0], ': ', message[1])

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
