import json

class ThrottleController :  

    def __init__(self) :
        self.lap = 0
        self.piece = 0
        self.min = 0.627
        self.max = 0.677
        self.start = 0.61
        self.throttle = self.start
        self.angle = 0.0
        
    def value(self) :
        return self.throttle
        
    def increase(self, amount) :   
        if (self.throttle < self.max) : 
            self.throttle += amount
            if (self.throttle >= self.max) :
                self.throttle = self.max
                print('floored')
        
    def decrease(self, amount) :
        if (self.throttle > self.min) :
            self.throttle -= amount
            if (self.throttle < self.min) :
                self.throttle = self.min
                print('coasting')
        
    # note: no protection here against out-of-range values
    def set_to(self, value) :
        self.throttle = value
        
    def react_to(self, data) :
        current_lap = data[0]['piecePosition']['lap']
        current_piece = data[0]['piecePosition']['pieceIndex']
        angle = data[0]['angle']
        if (self.piece < current_piece) :
            self.piece = current_piece
            print('(l:', current_lap, ',p:)', current_piece,)
        #if (current_lap > self.lap) :
            #self.lap = current_lap
            #self.increase(0.1)
            #print(data)
            #print(data[0])
            #print(data[0]['piecePosition'])
        # if angle increases    
        if (angle > self.angle) :
            # slow down
            self.decrease(0.035)           
        else :
            # otherwise, speed up
            self.increase(0.015)
        self.angle = angle
                
        

